pipeline {
    agent any
    stages {
        stage('Prepare workspace') {
          steps {
            sh 'rm -rf ./framework'
            sh 'git clone https://gitlab.com/doscontainer/framework.git framework'
          }
        }
        stage('Build') {
            steps {
                script {
                  dir('/usr/local/jenkins/workspace/DOSContainer/') {
                      def yamlFiles = findFiles(glob: '**/*.yaml')
                      yamlFiles.each { yamlFile ->
                        def yamlFileName = yamlFile.name

                        // Run build.sh with each yaml file as a parameter
                        dir('framework') {
                            sh "./build.sh ../${yamlFileName}"
                        }}
                   }
                }
            }
        }
        stage('Upload images') {
            steps {
                sshagent(credentials: ['jenkins-sshkey']) {
                    sh 'scp ~/doscontainer/*.zip jenkins@web.area536.com:/srv/www/dosk8s/disks/'
                }
            }
        }
        stage('Author website post') {
            steps {
                sshagent(credentials: ['jenkins-web-author']) {
                    sh 'rm -rf ./website'
                    sh 'git clone git@code.area536.com:Area536/website.git website'
                    sh '/mgt/template.py /usr/local/jenkins/workspace/DOSContainer /mgt/template.j2 > website/content/posts/doscontainer-releases.md'
                    sh 'cd website; git add content/posts/doscontainer-releases.md'
                    sh 'cd website; git commit -m autoupdate'
                    sh 'cd website; git push'
                }
            }
        }
    }
    post {
        // Clean after build
        always {
            cleanWs(cleanWhenNotBuilt: true,
                    deleteDirs: true,
                    disableDeferredWipeout: true,
                    notFailBuild: true,
                    patterns: [[pattern: '.gitignore', type: 'INCLUDE'],
                               [pattern: '.propsfile', type: 'EXCLUDE']])
        }
    }
}
